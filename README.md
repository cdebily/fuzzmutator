# fuzzmutator-rs
fuzzmutator-rs is a small library used to mutate fuzzing data created with
portabilty and performance in mind.
# Usage
Please refer to the [documentation](https://docs.rs/fuzzmutator/0.2.0/fuzzmutator/).